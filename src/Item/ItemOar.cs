using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vintagestory.API.MathTools;
using Vintagestory.API.Util;
using Vintagestory.API.Client;

namespace XRowboat
{
    public class ItemOar : Item
    {
        public static string NAME { get; } = "ItemOar";

        public override string GetHeldTpIdleAnimation(ItemSlot activeHotbarSlot, Entity forEntity, EnumHand hand)
        {
            // if mounted on rowboat, use special oar holding animations in config
            if ( forEntity is EntityAgent )
            {
                EntityAgent entityAgent = forEntity as EntityAgent;
                if ( entityAgent.MountedOn is EntityRowboatSeat )
                {
                    switch ( forEntity.WatchedAttributes.GetInt(XRowboatMod.ATTRIBUTE_ROWING, 0) )
                    {
                        case 1:
                            return XRowboatMod.Config.OarRowForwardAnimation;
                        case -1:
                            return XRowboatMod.Config.OarRowReverseAnimation;
                        default:
                            return XRowboatMod.Config.OarIdleAnimation;
                    }
                }
            }

            return null;
        }
    }
}