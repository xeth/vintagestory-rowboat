using System;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;

namespace XRowboat
{
    public static class MathUtils
    {
        public static Vec2d Vec2dFromYaw(float yawRad)
        {
            return new Vec2d(Math.Cos(yawRad), -Math.Sin(yawRad));
        }

        public static Vec3d Vec3dFromYaw(float yawRad)
        {
            return new Vec3d(Math.Cos(yawRad), 0.0, -Math.Sin(yawRad));
        }

        public static double Clamp(double x, double min, double max)
        {
            if ( x < min )
            {
                return min;
            }
            else if ( x > max )
            {
                return max;
            }
            else
            {
                return x;
            }
        }
    }
}